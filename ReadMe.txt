Bought 802.11n/g/b 150Mbps Mini USB2.0 Wireless WiFi Network Lan Card Adapter w/Antenna Wifi dongle. 
It uses Ralink MT7601 chipset not included in the Pidora build for Rapberry Pi.

http://www.raspberrypi.org/forums/viewtopic.php?t=49864

DPA_MT7601U_LinuxSTA_3.0.0.3_20130717_LS.tar.bz2
This is the only driver worked dor me (not from official site http://mediatek.com/en/downloads/).
In opposite to other drivers I tried, it have UTILS, NETIF, MODULE directories in the root directory and all hierarchy of files is little bit different.

In addition, I made some patches to source code in order to compile it on my Raspberry Pi.

To use it with NetworkManager
nmcli dev wifi con "myssid" password "myssidpassword"

Don't forget to copy file RT2870STA.dat from repository root to
/etc/Wireless/RT2870STA/RT2870STA.dat
Without it you will see interface but it will be non usable, with zeroed MAC e.t.c.

[2013-03-13]
Version 3.0.0.3
1. Fix Single-Sku and TSSI bug.
2. Support Multi-Channel feature.
3. Change ATELDE2PFROMBUF command to bufferWriteBack.
4. Support bufferLoadFromEfuse command to force enter buffer mode.
5. Support ATECALFREEINFO command to get calibration free info.
6. Support fast scan when ra0 or p2p0 is connected
7. Move IdleTimeout and StationKeepAlive profiles out to be supported in STA mode.
8. Fix P2P EAPOL packet using 1Mbps rate problem
9. Support wpa_cli p2p persistent group cmds
10. Fix ATE bug.
11. Fix bug when read channel power and BW delta power.
12. Fix crash issue in some platform when interface down.
13. Support HW PBC.
14. Fix bug in RTMP_TimerListRelease().
15. Support Xtal freuency offset compersation.
16. Update firmware to V1.5

[2013-01-23]
Version 3.0.0.2
1. Update firmware to V1.3
2. Add software protect for PLL lock.
3. Support new WOWLAN.
4. Fix p2p sigma 4.2.2(Version 5.0) fail.
5. Fix frequency tracking bug.
6. Fix ATELDE2P cannot work.
7. Fix scan in 802.11 power saving cause dead lock.
8. Update SINGLE_SKU_V2.
9. Support ATETEMP.
10. Support ch14 CCK OBW.
11. Fix p2p sigma test fail issue.
12. Support Calibration free and ATELDE2PFROMBUF.
13. Update VCO calibration procedure.
14. Fix temperature calibration bug at low temp.
15. Check DMA done bit while load FW.
16. Fix WirelessMode=5 cannot connect to 11n only AP.
17. Support iwpriv ra0 set ATETSSIDC=1 command

[2012-11-30]
Version 3.0.0.1
1. Change text type to UNIX.
2. Fixed OS_ABL support crash at Kernel 2.6.32.
3. Fixed ATELDE2P=1 crash.
4. Update frequency tracking thresholds.
5. Update firmware to beta 0.4
6. Update RF CR to 10121122.
7. Fixed BulkInCmdRspEvent may submit multiple bulk-in urb.
8. Support ATE feature.
9. Fixed wps fail with external registrar.
10. Support Miracast.

[2012-11-15]
Version 3.0.0.0
1. Support suspend/resume.
2. Support 802.11 power saving.
3. Support P2P.
4. Support antenna diversity.
5. Support microwave oven detection.

[2012-10-19]
Version 3.0.0.0-Beta7
1. MT7601STA initial version.
2. Support RX_CSO.
3. Support header translation.